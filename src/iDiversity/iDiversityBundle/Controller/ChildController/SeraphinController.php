<?php

namespace iDiversity\iDiversityBundle\Controller\ChildController;

use iDiversity\iDiversityBundle\Controller\iDiversityController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class SeraphinController extends iDiversityController
{
	/**
	* @Route("/dessin", name="dessin")
	*/
	public function dessin()
	{
		return $this->render('front/sandbox/dessin.html.twig', array());
		// return new Response('');
	}
}
