<?php

namespace iDiversity\iDiversityBundle\Controller\ChildController;

use iDiversity\iDiversityBundle\Controller\iDiversityController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class IntelligenceController extends iDiversityController
{
	/**
	* @Route("/intelligence", name="intelligence")
  * @ApiDoc(
  *  resource=true,
  *  description="Liste des intelligences",
  * )
	*/
    public function intelligenceList()
    {
      return $this->render('front/list/intelligence.html.twig', array('intelligences' => $this->getIntelligence()));
    }
}
