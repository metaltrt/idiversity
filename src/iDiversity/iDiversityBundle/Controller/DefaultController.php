<?php

namespace iDiversity\iDiversityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
	/**
	* @Route("/", name="indexAction")
	*/
    public function indexAction()
    {
        return $this->redirectToRoute('skillList');
    }
}
