<?php

namespace iDiversity\iDiversityBundle\Controller;

use iDiversity\iDiversityBundle\Controller\iDiversityController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class IntelligenceController extends iDiversityController
{
  /**
  * @Route("/list/intelligence", name="listIntelligence")
  */
    public function listKit()
    {
      return $this->render('front/list/intelligence.html.twig', array('intelligences' => $this->getIntelligence()));
    }
}
