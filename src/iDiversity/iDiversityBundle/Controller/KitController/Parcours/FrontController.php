<?php

namespace iDiversity\iDiversityBundle\Controller\KitController\Parcours;

use iDiversity\iDiversityBundle\Controller\KitController\KitController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use iDiversity\iDiversityBundle\Entity\Experience;


class FrontController extends KitController
{
  /**
  * @Route("/kit/parcours/step_0", name="parcours_0")
  */
  public function parcours_0()
  {
    return $this->render('front/kit/parcours/step_0.html.twig');
  }

  /**
  * @Route("/kit/parcours/step_1", name="parcours_1")
  */
  public function parcours_1()
  {
    return $this->render('front/kit/parcours/step_1.html.twig');
  }

  /**
  * @Route("/kit/parcours/step_2", name="parcours_2")
  */
  public function parcours_2()
  {
    return $this->render('front/kit/parcours/step_2.html.twig', array());
  }

  /**
  * @Route("/kit/parcours/step_3", name="parcours_3")
  */
  public function parcours_3()
  {
    return $this->render('front/kit/parcours/step_3.html.twig', array());
  }

  /**
  * @Route("/kit/parcours/step_4", name="parcours_4")
  */
  public function parcours_4()
  {
    return $this->render('front/kit/parcours/step_4.html.twig', array());
  }

  /**
  * @Route("/kit/parcours/step_5", name="parcours_5")
  */
  public function parcours_5()
  {
    return $this->render('front/kit/parcours/step_5.html.twig');
  }

  /**
  * @Route("/kit/parcours/step_6", name="parcours_6")
  */
  public function parcours_6()
  {
    return $this->render('front/kit/parcours/step_6.html.twig', array());
  }

  /**
  * @Route("/kit/parcours/step_7", name="parcours_7")
  */
  public function parcours_7()
  {
    return $this->render('front/kit/parcours/step_7.html.twig', array('data' => $_POST));
  }

  /**
  * @Route("/kit/parcours/step_8", name="parcours_8")
  */
  public function parcours_8()
  {
	$entityManager = $this->getDoctrine()->getManager();
	$query = $entityManager->createQuery('SELECT xp FROM iDiversity\iDiversityBundle\Entity\Experimentation xp
										  WHERE xp.pathName = :pathName')
						   ->setParameter('pathName', 'parcours');
	$experimentations = $query->getResult();

	$experience = new Experience();
	$experience->setUser($this->getUser())
			   ->setExperimentation($experimentations[0])
			   ->setTitle($_POST['title'])
			   ->setDate(new \DateTime($_POST['date']))
			   ->setDescription($_POST['description']);

	unset($_POST['title']);
	unset($_POST['date']);
	unset($_POST['description']);

	$experience->setResult(json_encode($_POST));

	$entityManager->persist($experience);
	$entityManager->flush();

    return $this->render('front/kit/parcours/step_8.html.twig', array());
  }

  /**
  * @Route("/kit/parcours/step_9", name="parcours_9")
  */
  public function parcours_9()
  {
    return $this->render('front/kit/parcours/step_9.html.twig', array());
  }

  /**
  * @Route("/kit/parcours/step_10", name="parcours_10")
  */
  public function parcours_10()
  {
    // $experiences = $this->getExperience();


			$entityManager = $this->getDoctrine()->getManager();
			$query = $entityManager->createQuery('SELECT ex FROM iDiversity\iDiversityBundle\Entity\Experience ex,
																iDiversity\iDiversityBundle\Entity\Experimentation xp
															WHERE ex.user = :user
															AND ex.experimentation = xp.id
															AND xp.pathName = :xp
															ORDER BY ex.id DESC')
															->setParameter('xp', 'parcours')
															->setParameter('user', $this->getUser());
			$experiences = $query->getResult();


    if (empty($experiences)) {
      return $this->render('front/kit/parcours/step_1.html.twig');
    }
    else {
      return $this->render('front/kit/parcours/step_10.html.twig', array('experiences' => $experiences));
    }
  }

	/**
	* @Route("/saveModification", name="saveModification")
	*/

	public function saveModification(){
		if($_POST!= null){
			$entityManager = $this->getDoctrine()->getManager();
			$query = $entityManager->createQuery('SELECT ex FROM iDiversity\iDiversityBundle\Entity\Experience ex
														  WHERE ex.user = :user
														  AND ex.id = :exId
														  ')
														  ->setParameter('user', $this->getUser()->getId())
														 ->setParameter('exId', $_POST['id']);
			$experience = $query->getResult()[0];
			unset($_POST['id']);
			$experience->setTitle($_POST['title'])
					   ->setDate(new \DateTime($_POST['date']))
					   ->setDescription($_POST['description']);
			$experience->setResult(json_encode($_POST['arr']));

			$entityManager->persist($experience);
			$entityManager->flush();
			unset($_POST);
			return new Response('done');
		}
		return new Response('fail');
	}

  /**
  * @Route("/kit/parcours/step_11", name="parcours_11")
  */
  public function parcours_11()
  {
  	$exId = $_GET['ex'];
	$entityManager = $this->getDoctrine()->getManager();
	$query = $entityManager->createQuery('SELECT ex FROM iDiversity\iDiversityBundle\Entity\Experience ex
													WHERE ex.user = :user
													AND ex.id = :exId
													')
													->setParameter('user', $this->getUser()->getId())
												   ->setParameter('exId', $exId);
	$experiences = $query->getResult();
	$emotions = json_decode($experiences[0]->getResult());
    return $this->render('front/kit/parcours/step_11.html.twig', array('experience' => $experiences[0], 'emotions' => $emotions, 'id' => $exId));
  }
}
