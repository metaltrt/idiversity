<?php

namespace iDiversity\iDiversityBundle\Controller\KitController\BlindTest1;

use iDiversity\iDiversityBundle\Controller\KitController\KitController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use iDiversity\iDiversityBundle\Entity\Experience;


class FrontController extends KitController
{
  /**
  * @Route("/kit/blind_test_1/step_0", name="blind_test_1_0")
  */
  public function blind_test_1_0()
  {
	$experimentation = $this->getExperimentationByName('blind_test_1');
    return $this->render('front/kit/blind_test_1/step_0.html.twig', array('experimentation' => $experimentation));
	// return new Response('ok');
  }

  /**
  * @Route("/kit/blind_test_1/step_1", name="blind_test_1_1")
  */
  public function blind_test_1_1()
  {
	$vids = $this->getVideosList();
	return $this->render('front/kit/blind_test_1/step_1.html.twig', array('vids'=>$vids));
  }

  /**
  * @Route("/kit/blind_test_1/step_2", name="blind_test_1_2")
  */
	public function blind_test_1_2()
	{
		$media_id = $_GET['vid'];
		if($media_infos = $this->getInfoVid($media_id))
		{
			$media_content=json_decode($media_infos['media_content'],true);
			// var_dump($media_content);
			// die();
			$vid_id_yt = $media_content['blind_test_1']['id_yt'];
			$start_tps = $media_content['blind_test_1']['tps'];

			return $this->render('front/kit/blind_test_1/step_2.html.twig', array('media_id'=>$media_id,'start_tps'=>$start_tps,'vid_id_yt'=>$vid_id_yt, 'media_name'=>$media_infos['media_name'], 'media_url'=>$media_infos['media_url']));
		}
		else
			return $this->render('front/kit/blind_test_1/step_2_err.html.twig', array());
	}

  /**
  * @Route("/kit/blind_test_1/step_3", name="blind_test_1_3")
  */
	public function blind_test_1_3()
	{
		$media_id = $_GET['vid'];
		if($media_infos = $this->getInfoVid($media_id))
		{
			$media_content=json_decode($media_infos['media_content'],true);
			$goodAns = $media_content['blind_test_1']['keyword'];
			$ans = $this->pickAns($goodAns,3);
			shuffle($ans);
			return $this->render('front/kit/blind_test_1/step_3.html.twig', array('media_id'=>$media_id, 'listAns'=>$ans, 'goodAns'=>$goodAns));
		}
		else
			return $this->render('front/kit/blind_test_1/step_2_err.html.twig', array());
	}

	/**
    * @Route("/kit/blind_test_1/validateBT", name="validateBT")
    */
	public function validateBT()
	{
		date_default_timezone_set('Europe/Paris');
		$entityManager = $this->getDoctrine()->getManager();
		// echo $_POST['choosenAns'];
		$media_id = $_POST['mediaID'];
		$propositions = $_POST['props'];
		$goodAns = $_POST['goodAns'];
		$choosenAns = $_POST['choosenAns'];
		$arr = array();
		$arr[date("Y-m-d H:i:s")] = ['propositions'=>$propositions,'goodAns'=>$goodAns,'choosenAns'=>$choosenAns];

		// $exps = $this->getExperienceByTitle($media_id);
		if($exps= $this->getExperienceByTitle($media_id))
		{
			$exp = $exps[0];
			$result = json_decode($exp->getResult(),true);
			$result = array_merge($result,$arr);
			$ligneJSON = json_encode($result);

			$exp->setResult($ligneJSON);
		}
		else
		{
			$ligneJSON = json_encode($arr);

			$query = $entityManager->createQuery('SELECT xp FROM iDiversity\iDiversityBundle\Entity\Experimentation xp
												  WHERE xp.pathName = :pathName')
								   ->setParameter('pathName', 'blind_test_1');
			$experimentations = $query->getResult();

			$exp = new Experience();
			$exp->setUser($this->getUser())
				->setExperimentation($experimentations[0]) //Note : A changer quand les niveaux seront implémentés !!
				->setTitle($media_id)
				->setDate(new \DateTime(date("Y-m-d H:i:s")))
				->setResult($ligneJSON);
		}
		// echo getType($exp);
		$entityManager->persist($exp);
		$entityManager->flush();

		return new Response('OK');
	}

  /**
  * @Route("/kit/blind_test_1/step_4", name="blind_test_1_4")
  */
  public function blind_test_1_4()
  {
   return $this->render('front/kit/blind_test_1/step_4.html.twig', array());
  }

}
