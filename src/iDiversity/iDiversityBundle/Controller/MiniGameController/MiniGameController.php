<?php

namespace iDiversity\iDiversityBundle\Controller\MiniGameController;

use iDiversity\iDiversityBundle\Controller\iDiversityController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use iDiversity\iDiversityBundle\Entity\Experience;

class MiniGameController extends iDiversityController
{
	/**
	* @Route("/minigame/save/color_1", name="saveMinigameColor1")
	*/
	public function saveMinigameColor1()
	{
		$ligneJSON = $_POST['info'];

		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT xp FROM iDiversity\iDiversityBundle\Entity\Experimentation xp
											  WHERE xp.pathName = :pathName')
								->setParameter('pathName', 'color_1');
		$experimentations = $query->getResult();

		$exp = new Experience();
		$exp->setUser($this->getUser())
			->setExperimentation($experimentations[0])
			->setTitle('minigame color_1')
			->setDate(new \DateTime(date("Y-m-d H:i:s")))
			->setResult($ligneJSON);

		$entityManager->persist($exp);
		$entityManager->flush();

		return new Response('');
	}
}
