<?php

namespace iDiversity\iDiversityBundle\Controller\MiniGameController\Color_1;

use iDiversity\iDiversityBundle\Controller\KitController\KitController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class FrontController extends KitController
{
  /**
  * @Route("/minigame/color_1/step_0", name="color_1_0")
  */
  public function color_1_0()
  {
    return $this->render('front/minigame/color_1/step_0.html.twig');
  }

  /**
  * @Route("/minigame/color_1/step_1", name="color_1_1")
  */
  public function color_1_1()
  {
    return $this->render('front/minigame/color_1/step_1.html.twig');
  }

  /**
  * @Route("/minigame/color_1/step_2", name="color_1_2")
  */
  public function color_1_2()
  {
    return $this->render('front/minigame/color_1/step_2.html.twig');
  }

  /**
  * @Route("/minigame/color_1/step_3", name="color_1_3")
  */
  public function color_1_3()
  {
    return $this->render('front/minigame/color_1/step_3.html.twig');
  }
  /**
  * @Route("/minigame/color_1/step_4", name="color_1_4")
  */
  public function color_1_4()
  {
    return $this->render('front/minigame/color_1/step_4.html.twig');
  }
  /**
  * @Route("/minigame/color_1/step_5", name="color_1_5")
  */
  public function color_1_5()
  {
    return $this->render('front/minigame/color_1/step_5.html.twig');
  }
  /**
  * @Route("/minigame/color_1/step_6", name="color_1_6")
  */
  public function color_1_6()
  {
    return $this->render('front/minigame/color_1/step_6.html.twig');
  }
  /**
  * @Route("/minigame/color_1/step_7", name="color_1_7")
  */
  public function color_1_7()
  {
	  $entityManager = $this->getDoctrine()->getManager();

	  $query = $entityManager->createQuery('SELECT ex FROM iDiversity\iDiversityBundle\Entity\Experience ex,
		  													iDiversity\iDiversityBundle\Entity\Experimentation xp
	  									  WHERE ex.user = :user
										  AND ex.experimentation = xp.id
										  AND xp.pathName = :pathName')
	  					   ->setParameter('user', $this->getUser())
	  					   ->setParameter('pathName', 'color_1');
	  $experiences = $query->getResult();

	  $data = array();
	  foreach($experiences as $key => $experience) {
		$data['experiences'] = $experience;
		$data['result'] = json_decode($experience->getResult());
	  }

	  // var_dump(count($experiences));
	  // die();

    return $this->render('front/minigame/color_1/step_7.html.twig', array('experiences' => $data));
  }
}
