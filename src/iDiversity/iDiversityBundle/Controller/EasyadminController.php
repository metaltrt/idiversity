<?php // src/Dataswati/PowerOPBundle/Controller/EasyadminController.php
namespace iDiversity\iDiversityBundle\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class EasyadminController extends BaseAdminController
{
	public function createNewUserEntity()
	{
			return $this->get('fos_user.user_manager')->createUser();
	}

	public function persistUserEntity($user)
	{
		$this->get('fos_user.user_manager')->updateUser($user, false);
		if($user->getPlainPassword() !== NULL) {
				$passwdEncoded = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
				$user->setPassword($passwdEncoded);
		}
		parent::persistEntity($user);
	}

	public function updateUserEntity($user)
	{
		if($user->getPlainPassword() !== NULL) {
				$passwdEncoded = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
				$user->setPassword($passwdEncoded);
		}
		parent::persistEntity($user);
	}
}
