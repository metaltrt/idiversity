<?php

namespace iDiversity\iDiversityBundle\Controller;

use iDiversity\iDiversityBundle\Controller\iDiversityController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class SkillController extends iDiversityController
{
  /**
  * @Route("/list/skill", name="skillList")
  */
    public function skillList()
    {
      return $this->render('front/list/skill.html.twig', array('skills' => $this->getSkill()));
    }
}
