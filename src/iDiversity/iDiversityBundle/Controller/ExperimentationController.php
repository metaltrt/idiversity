<?php

namespace iDiversity\iDiversityBundle\Controller;

use iDiversity\iDiversityBundle\Controller\iDiversityController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ExperimentationController extends iDiversityController
{
	/**
	* @Route("/list/kit", name="listKit")
	*/
	public function listKit()
	{
		$skill = $_GET['skill'];
		$experimentations = $this->getXP($skill);
		$skillObj = $this->getSkillByName($skill);
		return $this->render('front/list/experimentation.html.twig', array('experimentations' => $experimentations,
																		   'skill' => $skillObj));
	}
}
