<?php

namespace iDiversity\iDiversityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class iDiversityController extends Controller
{

	public function getSkill() {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT s FROM iDiversity\iDiversityBundle\Entity\Skill s');
		return $query->getResult();
	}

	public function getSkillByName($name) {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT s FROM iDiversity\iDiversityBundle\Entity\Skill s
											  WHERE s.pathName = :name')
							   ->setParameter('name', $name);
		return $query->getResult()[0];
	}

	public function getIntelligence() {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT i FROM iDiversity\iDiversityBundle\Entity\Intelligence i');
		return $query->getResult();
	}

	public function getExperimentation($skill) {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT xp FROM iDiversity\iDiversityBundle\Entity\Experimentation xp');
		return $query->getResult();
	}

	public function getExperimentationByName($name) {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT xp FROM iDiversity\iDiversityBundle\Entity\Experimentation xp
											  WHERE xp.pathName = :name')
							   ->setParameter('name', $name);
		return $query->getResult()[0];
	}

	public function getXP($skill) {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT xp FROM 	iDiversity\iDiversityBundle\Entity\SIXP sixp,
																iDiversity\iDiversityBundle\Entity\Experimentation xp,
																iDiversity\iDiversityBundle\Entity\Skill s
												WHERE s.pathName = :skill
												AND 	xp.id = sixp.experimentation
												AND 	sixp.skill = s.id')
												->setParameter('skill', $skill);
		return $query->getResult();
	}

	public function getExperience() {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT ex FROM iDiversity\iDiversityBundle\Entity\Experience ex
														WHERE ex.user = :user
														ORDER BY ex.id DESC')
														->setParameter('user', $this->getUser());
		return $query->getResult();
	}

	public function getVideosList() {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT media
											FROM iDiversity\iDiversityBundle\Entity\MXP mxp,
												iDiversity\iDiversityBundle\Entity\Media media,
												iDiversity\iDiversityBundle\Entity\Experimentation xp
											WHERE mxp.media = media.id
											AND media.type= :type
											AND mxp.experimentation=xp.id
											AND xp.pathName = :pathName')
											->setParameter('type', 'video')->setParameter('pathName','blind_test_1');

	  	$videos = $query->getResult();
		$vids = array();
		foreach ($videos as $key => $value) {
			$name = $value->getName();
			$media_id = $value->getId();
			$media_url = $value->getURL();
			$media_content = $value->getContent();
			$arr_content = json_decode($media_content,true);
			$vid_id_yt = $arr_content['blind_test_1']['id_yt'];
			$query = $entityManager->createQuery('SELECT xp
	    										  FROM iDiversity\iDiversityBundle\Entity\Experience xp,
												   		iDiversity\iDiversityBundle\Entity\Experimentation xpi
	    										  WHERE xp.experimentation = xpi.id
												  AND xpi.pathName=:pathName
	    										  AND xp.title = :mediaID
												  AND xp.user = :usr')
	    										  ->setParameter('pathName','blind_test_1')->setParameter('mediaID',$media_id)->setParameter('usr',$this->getUser());
	    	  $umxp = $query->getResult();
			  // $nb_try = (isset($umxp[0]))?count(json_decode($umxp[0]->getResult())):0;
			  $viewed = isset($umxp[0]);
			  $vids[]=['name'=>$name, 'viewed'=>$viewed,'media_id'=>$media_id, 'id_yt'=>$vid_id_yt,'media_content'=>$media_content, 'media_url'=>$media_url];
		}
		return $vids;
	}

	public function getInfoVid($media_id)
	{
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT media FROM iDiversity\iDiversityBundle\Entity\Media media
											WHERE media.id = :mediaID')->setParameter('mediaID',$media_id);
		$media = $query->getResult();

		if(!empty($media)) {
			$media_name = $media[0]->getName();
			$media_content = $media[0]->getContent();
			$media_url = $media[0]->getURL();
			$media_code = $media[0]->getCode();
			$media_type = $media[0]->getType();
			$media_description = $media[0]->getDescription();
			return array('media_name'=>$media_name,'media_content'=>$media_content, 'media_url'=>$media_url, 'media_code'=>$media_code, 'media_type'=>$media_type, 'media_description'=>$media_description);
		}
		else
			return false;
	}

	/**
	* @Route("/pickEmo", name="pickEmo")
	*/
	public function pickEmo()
	{
		$listEmo = file('https://idiversity.cc/appli/ressources_texte/emotions.txt');
		$nbEmo = count($listEmo);
		$proposedEmo = [];
		for($i=0;$i<4;$i++)
		{
			$rand = random_int(0, $nbEmo-1);
			$word = $listEmo[$rand];
			$word=preg_replace('#(.*)(\s)$#isU','$1',$word);
			if(in_array($word, $proposedEmo))
				$i--;
			else
				$proposedEmo[]=$word;
		}
		echo implode(',',$proposedEmo);
		return new Response('');
	}

	public function pickAns($goodAns, $nb)
	{
		//$nb correspond au nombre de réponse à tirer au hasard
		$listAns = file('https://idiversity.cc/appli/ressources_texte/situations.txt');
		$nbAns = count($listAns);
		$proposedAns = [$goodAns];
		for($i=0;$i<$nb;$i++)
		{
			$rand = random_int(0,$nbAns-1);
			$ans = preg_replace('#\n?\s?#isU','',$listAns[$rand]); //On supprime les éventuels espaces/retour à la ligne
			if(in_array($ans,$proposedAns))
				$i--;
			else
				$proposedAns[]=$ans;
		}
		// echo implode(',',$proposedAns);
		return $proposedAns;
	}

	public function getExperienceByTitle($title) {
		$entityManager = $this->getDoctrine()->getManager();
		$query = $entityManager->createQuery('SELECT ex FROM iDiversity\iDiversityBundle\Entity\Experience ex
														WHERE ex.user = :user
														AND ex.title = :title
														ORDER BY ex.id DESC')
														->setParameter('user', $this->getUser())->setParameter('title',$title);
		$ans = $query->getResult();
		if(!empty($ans))
			return $ans;
		else
			return false;
	}

}
