<?php // src/iDiversity/iDiversityBundle/Entity/Experimentation.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="experimentation")
 * @ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\ExperimentationRepository")
 */
class Experimentation {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\OneToMany(targetEntity="SIXP", mappedBy="experimentation")
	 * @ORM\OneToMany(targetEntity="UXPTrack", mappedBy="experimentation")
	 * @ORM\OneToMany(targetEntity="MXP", mappedBy="experimentation")
	 * @ORM\OneToMany(targetEntity="Experience", mappedBy="experimentation")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="pathName", type="string", length=255)
	 */
	private $pathName;


	public function __toString()
	{
		return $this->name;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Experimentation
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Experimentation
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set pathName
	 *
	 * @param string $pathName
	 * @return Experimentation
	 */
	public function setPathName($pathName)
	{
		$this->pathName = $pathName;
		return $this;
	}

	/**
	 * Get pathName
	 *
	 * @return string
	 */
	public function getPathName()
	{
		return $this->pathName;
	}

}
