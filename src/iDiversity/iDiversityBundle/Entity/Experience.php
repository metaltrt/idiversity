<?php // src/iDiversity/iDiversityBundle/Entity/Experience.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 	Skill
 *
 * 	@ORM\Table(name="experience")
 *	@ORM\Table(options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 * 	@ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\ExperienceRepository")
 */
class Experience {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
 	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
	private $user;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\Experimentation")
	 * @ORM\JoinColumn(name="experimentation_id", referencedColumnName="id")
	 */
	private $experimentation;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=255)
	 */
	private $title;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="result", type="text")
	 */
	private $result;


	public function __toString()
	{
		return (string) $this->id;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set user
	 *
	 * @param string $user
	 *
	 * @return UXPTrack
	 */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}

	/**
	 * Get user
	 *
	 * @return string
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * Set experimentation
	 *
	 * @param string $experimentation
	 *
	 * @return SIE
	 */
	public function setExperimentation($experimentation)
	{
		$this->experimentation = $experimentation;
		return $this;
	}

	/**
	 * Get experimentation
	 *
	 * @return string
	 */
	public function getExperimentation()
	{
		return $this->experimentation;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Experience
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return Experience
	 */
	public function setDate($date)
	{
		$this->date = $date;
		return $this;
	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Experience
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set result
	 *
	 * @param string $result
	 * @return Experience
	 */
	public function setResult($result)
	{
		$this->result = $result;

		return $this;
	}

	/**
	 * Get result
	 *
	 * @return string
	 */
	public function getResult()
	{
		return $this->result;
	}

}
