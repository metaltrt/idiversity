<?php // src/iDiversity/iDiversityBundle/Entity/Intelligence.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="intelligence")
 * @ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\IntelligenceRepository")
 */
class Intelligence {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
 	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\OneToMany(targetEntity="SIE", mappedBy="intelligence")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="logo", type="string", length=255,  nullable=true)
	 */
	private $logo;


	public function __toString()
	{
		return $this->name;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Intelligence
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Intelligence
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set logo
	 *
	 * @param string $logo
	 *
	 * @return Intelligence
	 */
	public function setLogo($logo)
	{
		$this->logo = $logo;
		return $this;
	}

	/**
	 * Get logo
	 *
	 * @return string
	 */
	public function getLogo()
	{
		return $this->logo;
	}

}
