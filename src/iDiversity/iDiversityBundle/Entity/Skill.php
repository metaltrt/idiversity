<?php // src/iDiversity/iDiversityBundle/Entity/Skill.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="skill")
 * @ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\SkillRepository")
 */
class Skill {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
 	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\OneToMany(targetEntity="SIE", mappedBy="skill")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="pathName", type="string", length=255)
	 */
	private $pathName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="logo", type="string", length=255,  nullable=true)
	 */
	private $logo;


	public function __toString()
	{
		return $this->name;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Skill
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set pathName
	 *
	 * @param string $pathName
	 * @return Skill
	 */
	public function setPathName($pathName)
	{
		$this->pathName = $pathName;
		return $this;
	}

	/**
	 * Get pathName
	 *
	 * @return string
	 */
	public function getPathName()
	{
		return $this->pathName;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Skill
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set logo
	 *
	 * @param string $logo
	 *
	 * @return Skill
	 */
	public function setLogo($logo)
	{
		$this->logo = $logo;
		return $this;
	}

	/**
	 * Get logo
	 *
	 * @return string
	 */
	public function getLogo()
	{
		return $this->logo;
	}

}
