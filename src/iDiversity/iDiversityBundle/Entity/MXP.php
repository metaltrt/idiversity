<?php // src/iDiversity/iDiversityBundle/Entity/MXP.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="mxp")
 * @ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\MXPRepository")
 */
class MXP {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
 	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\Media")
	 * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
	 */
	private $media;

	/**
	 * @var string
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\Experimentation")
	 * @ORM\JoinColumn(name="xp_id", referencedColumnName="id")
	 */
	private $experimentation;

	public function __toString()
	{
		$res = $this->experimentation . '-' . $this->media;
		return $res;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set media
	 *
	 * @param string $media
	 *
	 * @return MXP
	 */
	public function setMedia($media)
	{
		$this->media = $media;
		return $this;
	}

	/**
	 * Get media
	 *
	 * @return string
	 */
	public function getMedia()
	{
		return $this->media;
	}

	/**
	 * Set experimentation
	 *
	 * @param string $experimentation
	 *
	 * @return MXP
	 */
	public function setExperimentation($experimentation)
	{
		$this->experimentation = $experimentation;
		return $this;
	}

	/**
	 * Get experimentation
	 *
	 * @return string
	 */
	public function getExperimentation()
	{
		return $this->experimentation;
	}
}
