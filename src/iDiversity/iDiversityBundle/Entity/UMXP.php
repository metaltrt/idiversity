<?php // src/iDiversity/iDiversityBundle/Entity/UMXP.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="umxp")
 * @ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\MXPRepository")
 */
class UMXP {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
 	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\MXP")
	 * @ORM\JoinColumn(name="mxp_id", referencedColumnName="id")
	 */
	private $mxp;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
	private $user;

	/**
	 * @var int
	 *@ORM\Column(name="nb_try", type="integer", nullable=false)
	 */
	private $nb_try;

	public function __toString()
	{
		return $this->id;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set mxp
	 *
	 * @param string $mxp
	 *
	 * @return UMXP
	 */
	public function setMXP($mxp)
	{
		$this->mxp = $mxp;
		return $this;
	}

	/**
	 * Get mxp
	 *
	 * @return string
	 */
	public function getMXP()
	{
		return $this->mxp;
	}

	/**
	 * Set user
	 *
	 * @param string $user
	 *
	 * @return UMXP
	 */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}

	/**
	 * Get user
	 *
	 * @return string
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * Set nb_try
	 *
	 * @param int $nb_try
	 *
	 * @return UMXP
	 */
	public function setNbTry($nb_try)
	{
		$this->nb_try = $nb_try;
		return $this;
	}

	/**
	 * Get nb_try
	 *
	 * @return int
	 */
	public function getNbTry()
	{
		return $this->nb_try;
	}
}
