<?php // src/iDiversity/iDiversityBundle/Entity/SIXP.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="sixp")
 * @ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\SIXPRepository")
 */
class SIXP {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
 	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\Skill")
	 * @ORM\JoinColumn(name="skill_id", referencedColumnName="id")
	 */
	private $skill;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\Intelligence")
	 * @ORM\JoinColumn(name="intelligence_id", referencedColumnName="id")
	 */
	private $intelligence;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\Experimentation")
	 * @ORM\JoinColumn(name="experimentation_id", referencedColumnName="id")
	 */
	private $experimentation;


	public function __toString()
	{
		return $this->id;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set skill
	 *
	 * @param string $skill
	 *
	 * @return SIXP
	 */
	public function setSkill($skill)
	{
		$this->skill = $skill;
		return $this;
	}

	/**
	 * Get skill
	 *
	 * @return string
	 */
	public function getSkill()
	{
		return $this->skill;
	}

	/**
	 * Set intelligence
	 *
	 * @param string $intelligence
	 *
	 * @return SIXP
	 */
	public function setIntelligence($intelligence)
	{
		$this->intelligence = $intelligence;
		return $this;
	}

	/**
	 * Get intelligence
	 *
	 * @return string
	 */
	public function getIntelligence()
	{
		return $this->intelligence;
	}

	/**
	 * Set experimentation
	 *
	 * @param string $experimentation
	 *
	 * @return SIE
	 */
	public function setExperimentation($experimentation)
	{
		$this->experimentation = $experimentation;
		return $this;
	}

	/**
	 * Get experimentation
	 *
	 * @return string
	 */
	public function getExperimentation()
	{
		return $this->experimentation;
	}

}
