<?php

namespace iDiversity\iDiversityBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\AttributeOverrides({
 *	  @ORM\AttributeOverride(name="username", column=@ORM\Column(type="string", name="username", length=255, unique=false)),
 *	  @ORM\AttributeOverride(name="usernameCanonical", column=@ORM\Column(type="string", name="username_canonical", length=255, unique=false))
 * })
 */
class User extends BaseUser
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\OneToMany(targetEntity="UXTrack", mappedBy="fos_user")
	 */
	protected $id;

	/**
	* @ORM\Column(type="string", nullable=true)
	*/
   protected $apikey = null;

   /**
	* @var \DateTime
	*
	* @ORM\Column(name="date_key", type="datetime", nullable=true)
	*/
   private $dateKey;

	public function __construct()
	{
		parent::__construct();
		// your own logic
	}

	/**
	 * Get id
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * Set apikey
	 *
	 * @param string $apikey
	 *
	 * @return User
	 */
	public function setApikey($apikey)
	{
		$this->apikey = $apikey;

		return $this;
	}

	/**
	 * Get apikey
	 *
	 * @return string
	 */
	public function getApikey()
	{
		return $this->apikey;
	}

	/**
	 * Set dateKey
	 *
	 * @param \DateTime $dateKey
	 *
	 * @return Predict
	 */
	public function setDateKey($dateKey)
	{
		$this->dateKey = $dateKey;

		return $this;
	}

	/**
	 * Get dateKey
	 *
	 * @return \DateTime
	 */
	public function getDateKey()
	{
		return $this->dateKey;
	}
}
