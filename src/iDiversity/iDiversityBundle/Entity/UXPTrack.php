<?php // src/iDiversity/iDiversityBundle/Entity/UXPTrack.php

namespace iDiversity\iDiversityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 *
 * @ORM\Table(name="uxp_track")
 * @ORM\Entity(repositoryClass="iDiversity\iDiversityBundle\Repository\UXPTrackRepository")
 */
class UXPTrack {

	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 */
	private $user;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(targetEntity="iDiversity\iDiversityBundle\Entity\Experience")
	 * @ORM\JoinColumn(name="experience_id", referencedColumnName="id")
	 */
	private $experience;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", columnDefinition="enum('new', 'update')")
	 */
	private $type;


	public function __toString()
	{
		return (string) $this->id;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set user
	 *
	 * @param string $user
	 *
	 * @return UXPTrack
	 */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}

	/**
	 * Get user
	 *
	 * @return string
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * Set experience
	 *
	 * @param string $experience
	 *
	 * @return UXPTrack
	 */
	public function setExperience($experience)
	{
		$this->experience = $experience;
		return $this;
	}

	/**
	 * Get experience
	 *
	 * @return string
	 */
	public function getExperience()
	{
		return $this->experience;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 *
	 * @return UXPTrack
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

}
