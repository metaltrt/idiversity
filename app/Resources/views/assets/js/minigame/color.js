var GAME = {
	nbColor: 7,
	nbEssai: 3,
	essai: 1,
	points: 0,
	ratioLvl1: 1/2,
	info: [],

	runEssai: function() {
		GAME.runLvl1();
		GAME.updateEssai();
	},

	runLvl1: function() {
		GAME.updatePts();
		GAME.setColor('blanc');
		if(Math.random() < this.ratioLvl1) {
			var color = GAME.generateColor();
			GAME.setBackground(color);
			GAME.setText(color);
		} else {
			GAME.setBackground(GAME.generateColor());
			GAME.setText(GAME.generateColor());
		}
	},

	controlEssai: function(response) {
		var result = (GAME.getBackground() == GAME.getText()) ? 'yes' : 'no';
		this.essai++;
		GAME.updatePts();
		if(response == result) {
			$('#background_color').addClass('rebound');
			setTimeout(function() {
				$('#background_color').removeClass('rebound');
				GAME.nextEssai();
			}, 800);
		} else {
			$('#background_color').addClass('wizz');
			setTimeout(function() {
				$('#background_color').removeClass('wizz');
				GAME.nextEssai();
			}, 800);
		}
	},

	nextEssai: function() {
		if(this.essai > this.nbEssai) {
			url_redirect({url: '{{ path("color_1_4") }}',
				method: "GET"
			});
		} else {
			GAME.runEssai();
		}
	},

	controlLvl1: function(response) {
		var round = {};
		var result = (GAME.getBackground() == GAME.getText()) ? 'yes' : 'no';
		round.background = GAME.getBackground();
		round.color = GAME.getColor();
		round.text = GAME.getText();
		round.valid = response == result;
		this.info.push(round);

		if(response == result) {
			this.points++;
			$('#background_color').addClass('rebound');
			setTimeout(function() {
				$('#background_color').removeClass('rebound');
				GAME.updatePts();
				GAME.nextLvl1();
			}, 800);
		} else {
			$('#background_color').addClass('wizz');
			setTimeout(function() {
				$('#background_color').removeClass('wizz');
				GAME.nextLvl1();
			}, 800);
		}

	},

	nextLvl1: function() {
		if(this.points == 10) {
			var info = JSON.stringify(this.info);
			$.ajax({
				url: "{{ path('saveMinigameColor1')}}",
				data : {info},
				dataType : 'HTML',
				method: "POST",
				beforeSend: function( xhr ) {
					xhr.overrideMimeType( "text/plain; charset=UTF-8" );
				},
			})
			.done(function(data) {
				url_redirect({url: '{{ path("color_1_6") }}',
					method: "GET"
				});
			})
			.fail(function(xhr, textStatus, errorThrown) {
				console.log(xhr.responseText);
			});

		} else {
			GAME.runLvl1();
		}
	},

	generateColor: function() {
		var result = '';
		switch (Math.trunc(Math.random() * this.nbColor)) {
			case 0: result = 'rouge'; 	break;
			case 1: result = 'vert'; 	break;
			case 2: result = 'bleu';	break;
			case 3: result = 'violet'; 	break;
			case 4: result = 'marron'; 	break;
			case 5: result = 'jaune'; 	break;
			case 6: result = 'noir'; 	break;
		}
		return result;
	},

	setText: function(text) {
		text = text.charAt(0).toUpperCase() + text.slice(1);
		$('#text_color').html(text);
	},
	getText: function() {
		return $('#text_color').html().toLowerCase();
	},

	setColor: function(color) {
		var classes = $('#text_color').attr("class").split(' ');
		for (var attr of classes) {
			if(attr.indexOf('color') == 0) {
				$('#text_color').removeClass(attr);
			}
		}
		$('#text_color').addClass('color-' + color);
	},
	getColor: function() {
		var result = '';
		var classes = $('#text_color').attr("class").split(' ');
		for (var attr of classes) {
			if(attr.indexOf('color') == 0) {
				result = attr.replace('color-', '');
			}
		}
		return result;
	},

	setBackground: function(color) {
		var classes = $('#background_color').attr("class").split(' ');
		for (var attr of classes) {
			if(attr.indexOf('background') == 0) {
				$('#background_color').removeClass(attr);
			}
		}
		$('#background_color').addClass('background-' + color);
	},
	getBackground: function() {
		var result = '';
		var classes = $('#background_color').attr("class").split(' ');
		for (var attr of classes) {
			if(attr.indexOf('background') == 0) {
				result = attr.replace('background-', '');
			}
		}
		return result;
	},

	updateEssai: function() {
		$('#essai_number').html(this.essai);
	},

	updatePts: function() {
		$('#cpt-pts').html(this.points);
	},
};
