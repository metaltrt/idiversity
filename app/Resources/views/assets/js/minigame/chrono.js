var CHRONO = {

	start: 0,
	end : 0,
	diff :0,
	timerID : 0,

	msec : 0,
	sec : 0,
	min :0,

	ongoing:function(){
		this.end = new Date();
		this.diff = this.end - this.start;
		this.diff = new Date(this.diff);

		this.msec = this.diff.getMilliseconds();
		this.sec = this.diff.getSeconds();
		this.min = this.diff.getMinutes();
		if(this.min < 10){
			this.min = "0" + this.min
		}
		if(this.sec < 10){
			this.sec = "0" + this.sec
		}
		if(this.msec < 10){
			this.msec = "00" + this.msec
		}
		else if(this.msec < 100){
			this.msec = "0" + this.msec
		}

		document.getElementById("chrono").innerHTML = this.min + ':' + this.sec + ':' + this.msec;
		this.timerID = setTimeout("CHRONO.ongoing()",10);
	},

	startChrono:function(){
		this.start = new Date();
		this.ongoing();
	},

	pauseChrono:function(){
		clearTimeout(this.timerID);
	},

	resumeChrono:function(){
		this.start = new Date()-this.diff;
		this.start = new Date(this.start);
		this.ongoing();
	}
};
