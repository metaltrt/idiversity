<script>
var nbInput = 0;
var height=0;

window.onload = function()
{
	nbInput = $('.inputCount').length;
	height = window.innerHeight;
};

window.onresize = function()
{
	if(window.innerHeight<height)
		$('footer').hide();
	else
		$('footer').show();
};

function addEmo()
{
	nbInput = nbInput + 1;
	var newInput = '<div class="row mb-3" id="adj_' + nbInput + '" style="display:none;">\
						<input class="col col-10 inputCount" type="text" value=""/>\
						<div onclick="removeContent(\'adj_' + nbInput + '\')" title="Supprimer" class="col col-2 rm-emo btn-close"></div>\
					</div>';
	$("#form-list-emo").append(newInput);
	$("#adj_"+ nbInput).show('fast');
}

function popUpHelp()
{
	$('#btnSpin').addClass('spin');



	$('#popup-help').fadeIn(1000);
	$.ajax({
		url: "{{ path('pickEmo')}}",
		data : {},
		dataType : 'HTML',
		method: "POST",
		beforeSend: function( xhr ) {
			xhr.overrideMimeType( "text/plain; charset=UTF-8" );
		},
	})
	.done(function(data) {
		$('#btnSpin').removeClass('spin');

		var formAdj = $('#form-popup');
		var  adj = [];
		var id = [];
		var newAdj = data.split(',');
		for(var i = 0; i < 4; i++) {
		if (formAdj[0][i] != undefined) {
			if(formAdj[0][i].checked) {
					 adj.push(formAdj[0][i].title);
					 id[i] = 1;
				}
				else {
						 adj.push(newAdj[i]);
						 id[i] = 0;
					}
		}	else {
				 adj.push(newAdj[i]);
				 id[i] = 0;
			}
		}
		$('#form-popup').html('');

		for(var i = 0; i < adj.length; i++) {
			baliseAdj = '<input ';
			if (id[i] == 1) {
				baliseAdj += 'checked ';
			}
			baliseAdj += 'type="checkbox" name="emo' + i + '" id="emo' + i + '" title="' + adj[i] + '"> <label for="emo' + i + '">' + adj[i] + '</label>';
			$('#form-popup').append(baliseAdj);
		}
		$('#popup-help').show();
	})
	.fail(function(xhr, textStatus, errorThrown) {
		console.log(xhr.responseText);
	});
}


function popDownHelpClose()
{
	$('#popup-help').fadeOut(1000, function(){
		$('#form-popup').html('');
	});
}

function popDownHelp()
{
	var formAdj = $('#form-popup');
	var arrAdj = [];
	for(var i = 0; i < formAdj[0].length; i++) {
		if(formAdj[0][i].checked) {
			arrAdj.push(formAdj[0][i].title);
		}
	}

	var formList = $('#form-list-emo');
	var arrList = [];
	for(var i = 0; i < formList[0].length; i++) {
		if(formList[0][i].value !== "") {
			arrList.push(formList[0][i].value);
		}
	}

	var newArr = arrList.concat(arrAdj);
	var str = '';
	var index = newArr.length ;
	for (var i = 0; i < index; i++) {
		nbInput++;
		str += 		'<div class="row mb-3" id="adj_' + nbInput + '">\
							<input class="col col-10 inputCount" type="text" value="'+newArr[i]+'"/>\
							<div onclick="removeContent(\'adj_' + nbInput + '\')" title="Supprimer" class="col col-2 rm-emo btn-close"></div>\
						</div>';
	}

	$('#form-list-emo').html(str);
	$('#popup-help').fadeOut(1000, function(){
		$('#form-popup').html('');
	});
}

var CMD = {
	next: function() {
		var list = $('#form-list-emo');
		var arr = [];
		for(var i = 0; i < list[0].length; i++) {
			if(list[0][i].value !== "") {
				arr.push(list[0][i].value);
			}
		}
		url_redirect({url: '{{ path("parcours_7") }}',
		method: "POST",
		data: arr
		});
	}
}


function removeContent(idInput){
	$("#"+idInput).hide('fast', function(){
		$("#"+idInput).remove();
	});
}
</script>
