# Install Symfony 3.4 #

    sudo mkdir -p /usr/local/bin
    sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
    sudo chmod a+x /usr/local/bin/symfony
    symfony new my_project_name 3.4

# Install composer #

    sudo apt install unzip
    sudo apt install zip
    sudo apt install composer

# Création du Bundle #

    php bin/console generate:bundle

Activation du Bundle dans l'autoload du composer.json

```yml
    # composer.json
    "autoload": {
        "psr-4": {
        "": "src/"
        },
    },
```

    composer dump-autoload

Le namespace dans ce Controller doit être de la forme : **namespace Dataswati\PowerOPBundle\...** !

# Config Parameters #

```yml
    # app/config/parameters.yml
    parameters:
        database_host: mysql
        database_port: 3306
        database_name: db
        database_user: root
        database_password: 'password'

        mailer_transport: smtp
        mailer_host: 127.0.0.1
        mailer_user: email@example.com
        mailer_password: password
```

# Install FOS #

    composer require friendsofsymfony/user-bundle "~2.0"
    composer require symfony/templating

Ajouter le templating Twig dans la config.yml

```yml
    # app/config/config.yml
    framework:
        templating:
            engines: ['twig']
```

Activation du Bundle dans le Kernel

```php
    <?php // app/AppKernel.php
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new FOS\UserBundle\FOSUserBundle(),
        );
    }
```

Création de la DB

    php bin/console doctrine:database:create

Création de l'Entity User.php

    php bin/console generate:doctrine:entity

```php
    <?php // src/Dataswati/PowerOPBundle/Entity/User.php
    namespace Dataswati\PowerOPBundle\Entity;

    use FOS\UserBundle\Model\User as BaseUser;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * User
     *
     * @ORM\Table(name="fos_user")
     * @ORM\Entity(repositoryClass="Dataswati\PowerOPBundle\Repository\UserRepository")
     */
    class User extends BaseUser
    {
        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;


        public function __construct()
        {
            parent::__construct();
            // your own logic
        }

        /**
         * Get id
         *
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }
    }
```

Ajouter les modifications a la DB

    php bin/console doctrine:schema:update --dump-sql
    php bin/console doctrine:schema:update --force

Configuration security.yml

```yml
    # app/config/security.yml
    security:
    encoders:
        FOS\UserBundle\Model\UserInterface: bcrypt

    role_hierarchy:
        ROLE_ADMIN: ROLE_USER
        ROLE_SUPER_ADMIN: ROLE_ADMIN

    providers:
        fos_userbundle:
            id: fos_user.user_provider.username

    firewalls:
        main:
            pattern: ^/
            form_login:
                provider: fos_userbundle
                csrf_token_generator: security.csrf.token_manager
            logout: true
            anonymous: true

    access_control:
        - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
         - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/, role: ROLE_ADMIN }
```

Configuration config.yml

```yml
    # app/config/config.yml
    fos_user:
    db_driver: orm
    firewall_name: main
    user_class: Dataswati\PowerOPBundle\Entity\User
    from_email:
        address: "%mailer_user%"
        sender_name: "%mailer_user%"
```

Configuration routing.yml

```yml
    # app/config/routing.yml
    fos_user:
        resource: "@FOSUserBundle/Resources/config/routing/all.xml"
```

Mise à jour de DB avec les infos de FOS

    php bin/console doctrine:schema:update --dump-sql
    php bin/console doctrine:schema:update --force

Cache clear au moindre probleme de login

    php bin/console cache:clear --env=dev
    php bin/console cache:clear --env=prod
    sudo rm -rf var/cache/*

# Install Easy Admin #

    composer require easycorp/easyadmin-bundle

Activation du Bundle dans le Kernel

```php
    <?php // app/AppKernel.php
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new EasyCorp\Bundle\EasyAdminBundle\EasyAdminBundle(),
        );
    }
```

Configuration routing.yml

```yml
    # app/config/routing.yml
    easy_admin_bundle:
        resource: "@EasyAdminBundle/Controller/AdminController.php"
        type: annotation
        prefix: /backend
```

Install asset for Easy Admin

    php bin/console assets:install --symlink

Activation du translator

```yml
    # app/config/config.yml
    imports:
        - { resource: easyadmin/ }
        # ...

    framework:
        translator: { fallbacks: [ "en" ] }
        # ...
```

Création des fichiers de config easyadmin

    mkdir app/config/easyadmin

```yml
        # app/config/easyadmin/config.yml
    easy_admin:
        site_name: 'PowerOP'

        design:
            brand_color: '#e48d0d'
            menu:
                - User

        list:
            actions:
                - { name: 'edit', icon: 'pencil', label: false, title: 'Edit' }
                - { name: 'delete', icon: 'times', label: false, title: 'Delete' }
```

```yml
    # app/config/easyadmin/entities.yml
    easy_admin:
        entities:
            User:
                class: Dataswati\PowerOPBundle\Entity\User
                title: 'Users'
```

Installation du template easyadmin

    composer require "almasaeed2010/adminlte=~2.4"

## Connection des Bundles EasyAdmin et FOSUser ##

Création d'une extension du Controller de EasyAdmin

```php
    <?php // src/Dataswati/PowerOPBundle/Controller/EasyadminController.php
    namespace Dataswati\PowerOPBundle\Controller;

    use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

    class EasyadminController extends BaseAdminController
    {
        public function createNewUserEntity()
        {
            return $this->get('fos_user.user_manager')->createUser();
        }

        public function persistUserEntity($user)
        {
            $this->get('fos_user.user_manager')->updateUser($user, false);
            parent::persistEntity($user);
        }

        public function updateUserEntity($user)
        {
            parent::persistEntity($user);
        }
    }
```

Modification du fichier de routing vers le nouveau Controller

Ancienne configuration
```yml
    # app/config/routing.yml
    easy_admin_bundle:
        resource: "@EasyAdminBundle/Controller/AdminController.php"
        type: annotation
        prefix: /backend

    ========================= Convert to =========================

    easy_admin_bundle:
        resource: "@PowerOPBundle/Controller/EasyadminController.php"
        type: annotation
        prefix: /backend
```

Configuration de la liste des champs User

```yml
    # app/config/easyadmin/entities.yml
    # ...
    User:
        class: Dataswati\PowerOPBundle\Entity\User
        title: 'Users'
        list:
            fields:
                - { property: 'id', label: 'ID'}
                - { property: 'username', label: 'Username' }
                - { property: 'email', label: 'Email' }
                - { property: 'lastLogin', label: 'Last login'}
                - { property: 'roles', label: 'Role' }
                - { property: 'enabled', label: 'Enabled' }
        form:
            fields:
                - { property: 'username', label: 'Username' }
                - { property: 'email', label: 'Email' }
                - { property: 'roles', type: 'choice', type_options: { multiple: true, choices: { 'ROLE_USER': 'ROLE_USER', 'ROLE_API': 'ROLE_API', 'ROLE_ADMIN': 'ROLE_ADMIN' } } }
                - { property: 'enabled', label: 'Enabled' }
        edit:
            fields:
                - { property: 'plainPassword', label: 'Password', type: 'password', type_options: { required: false} }
        new:
            fields:
                - { property: 'plainPassword', label: 'Password', type: 'password', type_options: { required: true} }
    # ...
```

Encryptage du PlainPassword dans Password si il existe

```php
    <?php // src/Dataswati/PowerOPBundle/Controller/EasyadminController.php
    public function persistUserEntity($user)
    {
        $this->get('fos_user.user_manager')->updateUser($user, false);
        if($user->getPlainPassword() !== NULL) {
            $passwdEncoded = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($passwdEncoded);
        }
        parent::persistEntity($user);
    }

    public function updateUserEntity($user)
    {
        if($user->getPlainPassword() !== NULL) {
            $passwdEncoded = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($passwdEncoded);
        }
        parent::persistEntity($user);
    }
```

# Install Yarn + Node #

    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt install -y nodejs
    sudo npm install --global yarn

# Install Frontend Symfony #

    yarn add @symfony/webpack-encore --dev

    yarn add webpack-notifier --dev
    yarn add sass-loader node-sass --dev

    touch assets/js/app.js
    touch assets/css/app.css

```js
    // assets/js/app.js
    require('../css/app.css');

    // ...rest of JavaScript code here
```

    touch webpack.config.js

```js
    // webpack.config.js
    var Encore = require('@symfony/webpack-encore');

    Encore
    // the project directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    // will create web/build/app.js and web/build/app.css
    .addEntry('app', './assets/js/app.js')

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    // enable source maps during development
    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning()

    // allow sass/scss files to be processed
    .enableSassLoader()
    ;

    // export the final configuration
    module.exports = Encore.getWebpackConfig();
```

Compile assets
	yarn run encore dev

ReCompile assets automatically when files changes
	yarn run encore dev --watch

Compile, minify & optimize assets
	yarn run encore production

# Install Bootstrap #

	yarn add bootstrap --dev

Create new global.scss

```scss
    // assets/css/global.scss

    // customize some Bootstrap variables
    $primary: darken(#428bca, 20%);

    // the ~ allows you to reference things in node_modules
    @import "~bootstrap/scss/bootstrap";
```

    yarn add jquery --dev
    yarn add popper.js --dev

# Install API Authentication #

Update User Entity

```php
    <?php // src/Dataswati/PowerOPBundle/Entity/User.php
    //...

    /**
    * @ORM\Column(type="string",nullable=true)
    */
    protected $apikey = null;

    //...

    /**
     * Set apikey
     *
     * @param string $apikey
     *
     * @return User
     */
    public function setApikey($apikey)
    {
        $this->apikey = $apikey;

        return $this;
    }

    /**
     * Get apikey
     *
     * @return string
     */
    public function getApikey()
    {
        return $this->apikey;
    }

    //...
    ?>
```

Update Security

```yml
    # app/config/security.yml
    #...
    security:
        providers:
            #...
            api_rest:
                entity:
                    class: Dataswati\PowerOPBundle\Entity\User
                    property: apikey

        firewalls:
            #...
            api:
                pattern: ^/api
                # la conf pour l'api
                guard:
                    authenticators:
                        - Dataswati\PowerOPBundle\Security\TokenAuthenticator
                provider: api_rest
    #...
```

Create API security

```php
    <?php // src/Dataswati/PowerOPBundle/Security/TokenAuthenticator.php

    namespace Dataswati\PowerOPBundle\Security;

    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Response;

    use Symfony\Component\Security\Core\User\UserInterface;
    use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    use Symfony\Component\Security\Core\Exception\AuthenticationException;
    use Symfony\Component\Security\Core\User\UserProviderInterface;

    class TokenAuthenticator extends AbstractGuardAuthenticator
    {
        /**
         * Called on every request to decide if this authenticator should be
         * used for the request. Returning false will cause this authenticator
         * to be skipped.
         * Replaced by default null value to force authentification running even X-AUTH-TOKEN is missing
         */
        public function supports(Request $request)
        {
            $token_header = null;
            // $token_header = $request->headers->has('X-AUTH-TOKEN');
            $token_header = $request->headers->has('Authorization');

            return $token_header;
        }

        /**
         * Called on every request. Return whatever credentials you want to
         * be passed to getUser() as $credentials.
         */
        public function getCredentials(Request $request)
        {
            return array(
                // 'token' => $request->headers->get('X-AUTH-TOKEN'),
                'token' => $request->headers->get('Authorization'),
            );
        }

        public function getUser($credentials, UserProviderInterface $userProvider)
        {
            $apiKey = $credentials['token'];

            if (null === $apiKey) {
                return;
            }

            // if a User object, checkCredentials() is called
            return $userProvider->loadUserByUsername($apiKey);
        }

        public function checkCredentials($credentials, UserInterface $user)
        {
            // check credentials - e.g. make sure the password is valid
            // no credential check is needed in this case

            // return true to cause authentication success
            return true;
        }

        public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
        {
            // on success, let the request continue
            return null;
        }

        public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
        {
            $data = array(
                'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

                // or to translate this message
                // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
            );

            return new JsonResponse($data, Response::HTTP_FORBIDDEN);
        }

        /**
         * Called when authentication is needed, but it's not sent
         */
        public function start(Request $request, AuthenticationException $authException = null)
        {
            $data = array(
                // you might translate this message
                'message' => 'Authentication Required'
            );

            return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
        }

        public function supportsRememberMe()
        {
            return false;
        }
    }
```
Update service routing

```yml
    # app/config/security.yml
    #...
    Dataswati\PowerOPBundle\Security\:
        resource: '../../src/Dataswati/PowerOPBundle/Security'
    #...
```

# Install Neo4j #

## Install Bundle ##

```
$ composer require neo4j/neo4j-bundle
$ composer require graphaware/neo4j-php-ogm:@rc
```

## Update Kernel ##

```php
	<?php
	// app/AppKernel.php

	public function registerBundles()
	{
		$bundles = array(
			// ...
			new Neo4j\Neo4jBundle\Neo4jBundle(),
		);
	}
```

## Update Symfony config ##

```yml
    # app/config/config.yml
    # ...
    # Neo4j configuration
    neo4j:
    connections:
    default:
    scheme: "%neo4j_scheme%"
    host: "%neo4j_host%"
    port: "%neo4j_port%"
    username: "%neo4j_user%"
    password: "%neo4j_password%"
```

```yml
    # app/config/parameters.yml
    parameters:
        # ...
        neo4j_scheme: http
        neo4j_host: 172.32.0.6
        neo4j_port: 7474
        neo4j_user: neo4j
        neo4j_password: Swati&NeoBDD_4j!
        # ...
```
